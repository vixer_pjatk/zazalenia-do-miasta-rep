package com.example.andrew.problem;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Button;
import com.example.andrew.problem.Model.Problem;
import android.view.View.OnClickListener;


public class ProblemDetails extends Fragment {

    private Problem problem;

    public void setProblem(Problem problem) {
        this.problem = problem;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(this.problem.getTitle());

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_problem_details, container, false);

        TextView titleView = view.findViewById(R.id.problem_details_title);
        titleView.setText(this.problem.getTitle());

        TextView categoryView = view.findViewById(R.id.problem_details_category);
        categoryView.setText(this.problem.getCategory());

        TextView dateView = view.findViewById(R.id.problem_details_date);
        dateView.setText(this.problem.getCreatedAt());

        Button button = (Button) view.findViewById(R.id.problem_details_show_map_button);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ProblemMap mapFragment = new ProblemMap();
                mapFragment.setProblem(problem);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_frame_layout, mapFragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        return view;
    }
}
