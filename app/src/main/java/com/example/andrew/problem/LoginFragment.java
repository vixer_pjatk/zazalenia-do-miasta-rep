package com.example.andrew.problem;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class LoginFragment extends Fragment {

    private Fragment nextFragment = null;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Login");

        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_login, container, false);

        Button loginButton = this.view.findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getActivity().getSharedPreferences("login", Activity.MODE_PRIVATE);
                sp.edit().putBoolean("login", true).apply();

                if (nextFragment == null) {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_frame_layout, new AllProblems());
                    ft.addToBackStack(null);
                    ft.commit();
                } else {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_frame_layout, nextFragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });

        return this.view;
    }

    public void setNextFragment(Fragment nextFragment) {
        this.nextFragment = nextFragment;
    }
}
