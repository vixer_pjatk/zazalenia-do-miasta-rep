package com.example.andrew.problem.Model;


public class Problem {
    private String title;
    private String category;
    private String image;
    public double latitude;
    public double longitude;
    private String createdAt;

    public Problem(String title, String category, String image, double latitude, double longitude, String createdAt) {
        this.title = title;
        this.category = category;
        this.image = image;
        this.latitude = latitude;
        this.longitude = longitude;
        this.createdAt = createdAt;
    }

    public String getTitle() {
        return this.title;
    }

    public String getCategory() {
        return this.category;
    }

    public String getImage() {
        return this.image;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }
}
