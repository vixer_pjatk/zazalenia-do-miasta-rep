package com.example.andrew.problem;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import java.util.ArrayList;
import com.example.andrew.problem.Model.Problem;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class AllProblems extends Fragment {

    private View view;

    ArrayList<Problem> problems = new ArrayList<Problem>(){};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("All problems");

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_problems, container, false);

        this.view = view;

        ListView listView = view.findViewById(R.id.all_problems_list);

        /*String[] items = {"Apple", "Banan", "Grape"};
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(items));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.problem_list_item, R.id.problem_item_txt_view, arrayList);

        listView.setAdapter(adapter);*/

        this.problems.add(new Problem("Problem 1", "Category 1", "1.png", 52.249132, 21.008513, "18.12.2017"));
        this.problems.add(new Problem("Problem 2", "Category 3", "1.png", 52.269132, 21.028513, "04.01.2018"));
        this.problems.add(new Problem("Problem 3", "Category 3", "1.png", 52.289132, 21.008513, "06.01.2018"));
        this.problems.add(new Problem("Problem 4", "Category 2", "1.png", 52.309132, 21.058513, "08.01.2018"));
        this.problems.add(new Problem("Problem 5", "Category 1", "1.png", 52.329132, 21.088513, "10.01.2018"));
        this.problems.add(new Problem("Problem 6", "Category 2", "1.png", 52.349132, 21.018513, "10.01.2018"));
        this.problems.add(new Problem("Problem 7", "Category 4", "1.png", 52.369132, 21.028513, "11.01.2018"));
        this.problems.add(new Problem("Problem 8", "Category 4", "1.png", 52.389132, 21.018513, "17.01.2018"));
        this.problems.add(new Problem("Problem 8", "Category 1", "1.png", 52.509132, 21.098513, "17.01.2018"));

        StudentAdapter adapter = new StudentAdapter(getActivity(), this.problems);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Problem selectedProblem = problems.get(position);

            ProblemDetails problemDetailsFragment = new ProblemDetails();
            problemDetailsFragment.setProblem(selectedProblem);

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_frame_layout, problemDetailsFragment);
            ft.addToBackStack(null);
            ft.commit();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FloatingActionButton addProblemButton = (FloatingActionButton) this.view.findViewById(R.id.add_problem_float_button);
        addProblemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment fragment;

            if (getActivity().getSharedPreferences("login", Activity.MODE_PRIVATE).getBoolean("login", false)) {
                fragment = new AddProblem();
            } else {
                LoginFragment loginFragment = new LoginFragment();
                loginFragment.setNextFragment(new AddProblem());
                fragment = loginFragment;
            }

            ft.replace(R.id.fragment_frame_layout, fragment);
            ft.addToBackStack(null);
            ft.commit();
            }
        });
    }
}
