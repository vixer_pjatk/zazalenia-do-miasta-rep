package com.example.andrew.problem;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.andrew.problem.Model.Problem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class ProblemMap extends Fragment implements OnMapReadyCallback {

    private Problem problem;

    public void setProblem(Problem problem) {
        this.problem = problem;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(this.problem.getTitle());

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_problem_map, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.problem_map);
        mapFragment.getMapAsync(this);
    }

    // Include the OnCreate() method here too, as described above.
    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.addMarker(new MarkerOptions().position(new LatLng(this.problem.getLatitude(), this.problem.getLongitude()))
                .title(problem.getTitle()));

        float zoomLevel = 10.5f;
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(this.problem.getLatitude(), this.problem.getLongitude()), zoomLevel));
    }
}
