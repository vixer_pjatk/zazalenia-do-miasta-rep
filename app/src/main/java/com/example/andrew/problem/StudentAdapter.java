package com.example.andrew.problem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.app.Activity;
import android.widget.TextView;

import java.util.ArrayList;
import com.example.andrew.problem.Model.Problem;

public class StudentAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<Problem> problems;
    private static LayoutInflater inflater = null;

    public StudentAdapter(Activity context, ArrayList<Problem> problems) {
        this.activity = context;
        this.problems = problems;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.problems.size();
    }

    @Override
    public Object getItem(int position) {
        return this.problems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        itemView = (itemView == null) ? inflater.inflate(R.layout.problem_list_item, null) : itemView;
        TextView textViewTitle = (TextView) itemView.findViewById(R.id.problem_item_title_view);
        TextView textViewDate = (TextView) itemView.findViewById(R.id.problem_item_date_view);
        TextView textViewCategory = (TextView) itemView.findViewById(R.id.problem_item_category_view);

        Problem selectedProblem = this.problems.get(position);
        textViewTitle.setText(selectedProblem.getTitle());
        textViewDate.setText(selectedProblem.getCreatedAt());
        textViewCategory.setText(selectedProblem.getCategory());

        return itemView;
    }
}
