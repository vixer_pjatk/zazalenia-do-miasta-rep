package com.example.andrew.problem;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.andrew.problem.Model.Problem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class Map extends Fragment implements OnMapReadyCallback {

    ArrayList<Problem> problems = new ArrayList<Problem>(){};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Map");

        this.problems.add(new Problem("Problem 1", "Category 1", "1.png", 52.249132, 21.008513, "18.12.2017"));
        this.problems.add(new Problem("Problem 2", "Category 3", "1.png", 52.269132, 21.028513, "04.01.2018"));
        this.problems.add(new Problem("Problem 3", "Category 3", "1.png", 52.289132, 21.008513, "06.01.2018"));
        this.problems.add(new Problem("Problem 4", "Category 2", "1.png", 52.309132, 21.058513, "08.01.2018"));
        this.problems.add(new Problem("Problem 5", "Category 1", "1.png", 52.329132, 21.088513, "10.01.2018"));
        this.problems.add(new Problem("Problem 6", "Category 2", "1.png", 51.749132, 21.018513, "10.01.2018"));
        this.problems.add(new Problem("Problem 7", "Category 4", "1.png", 51.769132, 21.028513, "11.01.2018"));
        this.problems.add(new Problem("Problem 8", "Category 4", "1.png", 52.389132, 21.018513, "17.01.2018"));
        this.problems.add(new Problem("Problem 8", "Category 1", "1.png", 51.909132, 21.098513, "17.01.2018"));

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map2);
        mapFragment.getMapAsync(this);
    }

    // Include the OnCreate() method here too, as described above.
    @Override
    public void onMapReady(GoogleMap googleMap) {
        for (Problem problem : this.problems) {
            googleMap.addMarker(new MarkerOptions().position(new LatLng(problem.getLatitude(), problem.getLongitude()))
                    .title(problem.getTitle()));
        }

        float zoomLevel = 10.5f;
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(52.289132, 21.008513), zoomLevel));
    }
}
