package com.example.andrew.problem;

import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View.OnClickListener;
import android.content.Intent;
import android.app.Activity;
import android.net.Uri;
import android.widget.Spinner;

import java.util.ArrayList;


public class AddProblem extends Fragment implements OnClickListener {

    private static final int RESULT_LOAD_IMAGE = 1;

    private View view;
    private ImageView imageToUploadView;
    private Button selectImageButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Add problem");

        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_add_problem, container, false);

        ArrayList<String> categories = new ArrayList<String>();
        categories.add("Category 1");
        categories.add("Category 2");
        categories.add("Category 3");
        categories.add("Category 4");
        categories.add("Category 5");
        categories.add("Category 6");
        categories.add("Category 7");

        Spinner categorySpinner = view.findViewById(R.id.category_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.category_spinner_item, R.id.category_spinner_item_title, categories);
        categorySpinner.setAdapter(adapter);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.imageToUploadView = this.view.findViewById(R.id.image_to_upload);
        this.selectImageButton = this.view.findViewById(R.id.select_image_button);

        this.imageToUploadView.setOnClickListener(this);
        this.selectImageButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_to_upload:

                break;
            case R.id.select_image_button:
                Intent galeryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galeryIntent, RESULT_LOAD_IMAGE);

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && data != null) {
            Uri selectedImage = data.getData();
            this.imageToUploadView.setImageURI(selectedImage);
        }
    }
}
